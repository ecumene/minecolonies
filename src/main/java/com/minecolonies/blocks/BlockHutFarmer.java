package com.minecolonies.blocks;

public class BlockHutFarmer extends BlockHut
{
    protected BlockHutFarmer()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutFarmer";
    }
}